// La lógica y estructura de este programa está basado en la figura 18.9,  pag 788 del libro Cómo Programar en Java. (10th ed.). Deitel y Deitel (2016). 
//México: Pearson Educación. Capítulo 18. Sección 18.7
//Se lo ha adaptado o cambiado para que funcione usando la clase BigInteger, y así poder realizar las pruebas respectivas, ya que
//se van a generar miles de factoriales - 7400 en este caso -  para probar el rendimiento del procesador, uso de memoria, etc.
//Fecha de creación del algoritmo y de realización de las pruebas:  Viernes, 3 de Julio de 2020.  Guayaquil-Ecuador.
//Autor:  Camilo Javier García Tobar email:camilogarciatobar@hotmail.com

//Método factorial iterativo usando la clase BigInteger
import java.math.BigInteger;

public class FactorialCalculatorIteBigInt
{
   // Declaración recursiva del método factorial   
   public static BigInteger factorial(BigInteger number)
   {
      BigInteger result = BigInteger.ONE;

	  //Declaracion iterativa de metodo factorial
	  for (BigInteger i = number; (i.compareTo(BigInteger.ONE) >= 0); i=i.subtract(BigInteger.ONE))
		result = result.multiply(i);
	  return result;
   } 

   //Salida de los factoriales para los valores 0-8000
   public static void main(String[] args)
   {
	  //Calcula los factoriales del 0 al 7400
	  for (int counter = 0; counter <= 7400; counter++)
         System.out.printf("%d! = %d%n", counter, factorial(BigInteger.valueOf(counter)));
   }
} // Fin de la clase FactorialCalculatorIteBigInt
 

 
